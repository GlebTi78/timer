using System;
using UnityEngine;

public class ApplicationController : MonoBehaviour
{
    public static event Action OnFixedUpdate;
    public static event Action OnSceneFinished;

    private AudioSource audioSource;
    private TimerController timer;
    private float timerDuration = 5f;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        
        timer = new TimerController(timerDuration);
        timer.OnTimerFinished += TimerFinished;
    }

    private void OnDestroy()
    {
        OnSceneFinished.Invoke();
        timer.OnTimerFinished -= TimerFinished;
    }

    private void FixedUpdate()
    {
        OnFixedUpdate?.Invoke();
    }

    private void TimerFinished()
    {
        audioSource.Play();
    }
}
