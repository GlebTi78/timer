using System;

public class TimerController
{
    public static event Action<float> OnTimerUpdate;
    public event Action OnTimerFinished;

    private float duration;
    private float tickValue = 0.02f;

    public TimerController(float duration)
    {
        this.duration = duration;
        
        ApplicationController.OnFixedUpdate += TimerUpdate;
        ApplicationController.OnSceneFinished += Dispose;
    }

    private void Dispose()
    {
        ApplicationController.OnFixedUpdate -= TimerUpdate;
        ApplicationController.OnSceneFinished -= Dispose;
    }

    private void TimerUpdate()
    {
        duration -= tickValue;

        if (duration <= 0)
        {
            duration = 0;
            OnTimerFinished.Invoke();
            ApplicationController.OnFixedUpdate -= TimerUpdate;
        }

        OnTimerUpdate.Invoke(duration);
    }
}
