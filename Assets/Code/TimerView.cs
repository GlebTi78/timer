using UnityEngine;
using UnityEngine.UI;

public class TimerView : MonoBehaviour
{
    [SerializeField] private Text timerText;

    private void Start()
    {
        TimerController.OnTimerUpdate += UpdateView;
    }

    private void OnDestroy()
    {
        TimerController.OnTimerUpdate -= UpdateView;
    }

    private void UpdateView(float timeValue)
    {
        float minutes = Mathf.FloorToInt(timeValue / 60);
        float seconds = Mathf.FloorToInt(timeValue % 60);
        timerText.text = string.Format("{0:00} : {1:00}", minutes, seconds);
    }
}
